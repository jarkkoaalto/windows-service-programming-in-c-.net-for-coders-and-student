﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Section02
{
    public partial class UdemyWindowsService : ServiceBase
    {
        public UdemyWindowsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            EventLog.WriteEntry("Udemy Window Service by Jarkko Aalto is starting", EventLogEntryType.Information);
     
        }

        protected override void OnStop()
        {
            EventLog.WriteEntry("Udemy Window Service by Jarkko Aalto is stoping", EventLogEntryType.Information);
        }
    }
}
