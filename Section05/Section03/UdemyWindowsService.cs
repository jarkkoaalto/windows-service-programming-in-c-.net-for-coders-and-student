﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Threading;
using System.IO;

namespace Section03
{
    public partial class UdemyWindowsService : ServiceBase
    {
        ILog mLogger;

        Timer mRepeatingTimer;
        double mCounter;

        FileSystemWatcher mFSW;

        public UdemyWindowsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            EventLog.WriteEntry("Udemy Windows Service by Jarkko Aalto is starting", EventLogEntryType.Information);
            ConfigureLog4Net();

            int i = 0;
            System.Diagnostics.Debugger.Launch();
            do
            {
                System.Diagnostics.Debug.WriteLine(string.Format("Value of i is: {0}", i));
                mLogger.Debug(string.Format("Value of i is: {0}", i));

            } while (i++ < 5);

            mLogger.Error("This is an error.");
            int nTimerDelay = Properties.Settings.Default.TimerDelay;
            mLogger.Debug(string.Format("Value of TimerDelay is: {0}", nTimerDelay));

            mRepeatingTimer = new Timer(myTimerCallback, mRepeatingTimer, nTimerDelay, nTimerDelay);
            try { 
            mFSW = new FileSystemWatcher(@"C:\Users\Jarkko\Desktop\Udemy\GITREPOS\windows-service-programming-in-c-.net-for-coders-and-student\Section04\Section03\bin\Debug\Watch Me");
            mFSW.EnableRaisingEvents = true;
            mFSW.IncludeSubdirectories = true;

            mFSW.Created += MFSW_somethingHappendToTheFolder;
            mFSW.Changed += MFSW_somethingHappendToTheFolder;
            mFSW.Deleted += MFSW_somethingHappendToTheFolder;
            mFSW.Renamed += MFSW_somethingHappendToTheFolder;
            }catch(Exception e)
            {
                Console.WriteLine(e);
            }

        }

        // private void MFSW_Created(object sender, FileSystemEventArgs e) Original
        private void MFSW_somethingHappendToTheFolder(object sender, FileSystemEventArgs e)
        {
            mLogger.Debug(string.Format("\r\n{0} - \r\n{1}", e.ChangeType, e.FullPath));
            Debug.WriteLine(string.Format("\r\n{0} - \r\n{1}", e.ChangeType, e.FullPath));
        }

        protected override void OnStop()
        {
            EventLog.WriteEntry("Udemy Windows Service by Jarkko Aalto is stopping", EventLogEntryType.Information);
        }


        private void ConfigureLog4Net()
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                mLogger = LogManager.GetLogger("servicelog");
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ex.Message, EventLogEntryType.Error);
            }

        }
        public void myTimerCallback(object objParam)
        {
            mLogger.Debug(string.Format("Value of counter is: {0}", mCounter++));
            System.Diagnostics.Debug.WriteLine(string.Format("Value of counter is: {0}", mCounter));
        }
    }
}
