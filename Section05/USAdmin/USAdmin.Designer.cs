﻿using System;

namespace USAdmin
{
    partial class USAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetServiceStatus = new System.Windows.Forms.Button();
            this.lblServiceStatus = new System.Windows.Forms.Label();
            this.btnStopService = new System.Windows.Forms.Button();
            this.btnStartService = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGetServiceStatus
            // 
            this.btnGetServiceStatus.Location = new System.Drawing.Point(163, 226);
            this.btnGetServiceStatus.Name = "btnGetServiceStatus";
            this.btnGetServiceStatus.Size = new System.Drawing.Size(109, 23);
            this.btnGetServiceStatus.TabIndex = 0;
            this.btnGetServiceStatus.Text = "Get Service Status";
            this.btnGetServiceStatus.UseVisualStyleBackColor = true;
            this.btnGetServiceStatus.Click += new System.EventHandler(this.btnGetServiceStatus_Click);
            // 
            // lblServiceStatus
            // 
            this.lblServiceStatus.AutoSize = true;
            this.lblServiceStatus.Location = new System.Drawing.Point(13, 236);
            this.lblServiceStatus.Name = "lblServiceStatus";
            this.lblServiceStatus.Size = new System.Drawing.Size(83, 13);
            this.lblServiceStatus.TabIndex = 1;
            this.lblServiceStatus.Text = "lblServiceStatus";
            // 
            // btnStopService
            // 
            this.btnStopService.Location = new System.Drawing.Point(163, 185);
            this.btnStopService.Name = "btnStopService";
            this.btnStopService.Size = new System.Drawing.Size(109, 23);
            this.btnStopService.TabIndex = 2;
            this.btnStopService.Text = "Stop Service";
            this.btnStopService.UseVisualStyleBackColor = true;
            this.btnStopService.Click += new System.EventHandler(this.btnStopService_Click);
            // 
            // btnStartService
            // 
            this.btnStartService.Location = new System.Drawing.Point(163, 144);
            this.btnStartService.Name = "btnStartService";
            this.btnStartService.Size = new System.Drawing.Size(109, 23);
            this.btnStartService.TabIndex = 3;
            this.btnStartService.Text = "Start Service";
            this.btnStartService.UseVisualStyleBackColor = true;
            this.btnStartService.Click += new System.EventHandler(this.btnStartService_Click);
            // 
            // USAdmin
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnStartService);
            this.Controls.Add(this.btnStopService);
            this.Controls.Add(this.lblServiceStatus);
            this.Controls.Add(this.btnGetServiceStatus);
            this.Name = "USAdmin";
            this.Text = "Udemy Service Status";
            this.Load += new System.EventHandler(this.USAdmin_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void Udemy(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Label lblServiceStatus;
        private System.Windows.Forms.Button btnGetServiceStatus;
        private System.Windows.Forms.Button btnStopService;
        private System.Windows.Forms.Button btnStartService;

        public EventHandler label1 { get; private set; }
        // private System.Windows.Forms.Label label1;
    }
}

