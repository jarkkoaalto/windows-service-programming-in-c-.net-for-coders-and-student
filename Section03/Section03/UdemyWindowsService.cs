﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Section03
{
    public partial class UdemyWindowsService : ServiceBase
    {
        ILog mLogger;

        public UdemyWindowsService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            EventLog.WriteEntry("Udemy Windows Service by Jarkko Aalto is starting", EventLogEntryType.Information);
            ConfigureLog4Net();

            int i = 0;
            System.Diagnostics.Debugger.Launch();
            do
            {
                System.Diagnostics.Debug.WriteLine(string.Format("Value of i is: {0}", i));
                mLogger.Debug(string.Format("Value of i is: {0}", i));

            } while (i++ < 5);

            mLogger.Error("This is an error.");


        }

        protected override void OnStop()
        {
            EventLog.WriteEntry("Udemy Windows Service by Jarkko Aalto is stopping", EventLogEntryType.Information);
        }


        private void ConfigureLog4Net()
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                mLogger = LogManager.GetLogger("servicelog");
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry(ex.Message, EventLogEntryType.Error);
            }

        }
    }
}
